#!/bin/bash

# see https://stackoverflow.com/questions/7261855/recommendation-for-compressing-jpg-files-with-imagemagick#7262050
# requires ImageMagick, please install it with the package manager of your choice.

for f in raw/* ; do
    name=$(echo "$f" | cut -f 2 -d '/' | cut -f 1 -d '.')
    echo $f
    magick $f -strip -interlace Plane -gaussian-blur 0.05 -quality 85% -resize 40% -ordered-dither 2x2 images/$name.jpg
done